from flask.ext.wtf import Form
from wtforms import validators, StringField, TextAreaField


class AdminForm(Form):
    email = StringField(u"email", validators=[validators.DataRequired(message=u"Email is required"),
                                              validators.Email(message=u"Invalid email")])


class NewsForm(Form):
    name = StringField(u"name", validators=[validators.DataRequired(message=u"This field is required!")])
    body = TextAreaField(u"description", validators=[validators.DataRequired(u"This field is required!")])