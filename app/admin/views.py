from flask import Blueprint, render_template, request, json, jsonify, abort
from app.decorators import *
from app.admin.forms import *
from app.models import Admin

admin = Blueprint('admin', __name__, url_prefix='/admin', template_folder='templates')


@admin.route('/', methods=['GET', 'POST'])
@only_admin
def admin_index():
    return render_template('admin-base.html', admins=Admin.all(), admin_form=AdminForm())


@admin.route('/add_admin', methods=['GET', 'POST'])
@only_admin
def add_admin():
    admin_form = AdminForm(request.form)
    key = ''
    if admin_form.validate():
        admin = Admin(email=admin_form.email.data)
        if Admin.all().count() >= 2:
            return json.dumps({'valid': False})
        admin.put()
        key = admin.key()
    return json.dumps({'valid': admin_form.validate_on_submit(), 'key': str(key), 'email': admin_form.email.data})


@admin.route('/news', methods=['GET', 'POST'])
@only_admin
def admin_news():
    return render_template('admin-news.html', news=News.all().get())


@admin.route('/delete_by_key/<string:key>', methods=['GET', 'POST'])
@only_admin
def delete_by_key(key):
    db.delete(key)
    return json.dumps({})


@admin.route('/add_faculty/')
@only_admin
def add_faculty():
    return render_template('admin-faculty.html')


@admin.route('/articles')
@only_admin
def articles():
    return render_template('admin-articles.html', blogs=db.Query(Blog).order("-date"), groups=GroupsOfBlogs.all())


@admin.route('/blog/group/<int:group_id>', methods=['POST'])
@only_admin
def blog_group_get(group_id):
    return jsonify(group=GroupsOfBlogs.get(group_id))


@admin.route('/blog/group/all', methods=['POST'])
@only_admin
def blog_group_get_all():
    return jsonify(groups=GroupsOfBlogs.get().all())


@admin.route('/blog/group/add', methods=['POST'])
@only_admin
def blog_group_add():
    data = json.loads(request.data)

    try:
        group = GroupsOfBlogs(name=data["name"])
    except (ValueError, KeyError, TypeError):
        return abort(400)

    group.put()

    return json.dumps({"key": group.key().id()})


@admin.route('/blog/group/<int:group_id>/edit', methods=['POST'])
@only_admin
def blog_group_edit(group_id):
    data = json.loads(request.data)
    group = GroupsOfBlogs.get_by_id(group_id)

    try:
        group.name = data["name"]
    except (ValueError, KeyError, TypeError):
        return abort(400)

    group.put()

    return json.dumps({})


@admin.route('/blog/group/<int:group_id>/delete', methods=['POST'])
@only_admin
def blog_group_delete(group_id):
    blogs_by_group = Blog.all().filter('group=', GroupsOfBlogs.get_by_id(long(group_id)))

    if blogs_by_group.count():
        return json.dumps({"msg": "Can't delete this group, because have blogs with that group"})

    group = GroupsOfBlogs.get_by_id(group_id)
    group.delete()

    return json.dumps({})


# page: admin/blog
@admin.route('/blog', methods=['GET'])
@only_admin
def blog():
    return render_template("admin-articles.html", blogs=db.Query(Blog).order("-date"), groups=GroupsOfBlogs.all())


@admin.route('/blog/<int:blog_id>', methods=['POST'])
@only_admin
def blog_get(blog_id):
    return jsonify(blog=Blog.get(blog_id))


@admin.route('/blog/<int:blog_id>/amount/<int:amount_blogs>')
@only_admin
def blog_get_certain_amount(blog_id, amount_blogs):
    return jsonify(blogs=Blog.all().order("-data").ancestor(blog_id).fetch(limit=amount_blogs))


@admin.route('/blog/add', methods=['POST'])
@only_admin
def blog_add():
    data = json.loads(request.data)

    Blog(name=data["name"], content=data["content"], group=GroupsOfBlogs.get_by_id(long(data["group"]))).put()

    return json.dumps({})


@admin.route('blog/<int:blog_id>/edit', methods=['POST'])
@only_admin
def blog_edit(blog_id):
    data = json.loads(request.data)
    blog = Blog.get_by_id(blog_id)

    try:
        blog.name = data["name"]
        blog.content = data["content"]
        blog.group = GroupsOfBlogs.get_by_id(long(data["group"]))

        blog.put()
    except (ValueError, KeyError, TypeError):
        return abort(400)

    return json.dumps({})


@admin.route('blog/<int:blog_id>/delete', methods=['POST'])
@only_admin
def blog_delete(blog_id):
    Blog.get_by_id(blog_id).delete()
    return json.dumps({})