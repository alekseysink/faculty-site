from google.appengine.ext import db


class Admin(db.Model):
    email = db.EmailProperty(required=True)


class Faculty(db.Model):
    name = db.StringProperty(required=True)
    description = db.TextProperty(required=True)
    logo = db.BlobProperty(required=False)


class News(db.Model):
    faculty = db.ReferenceProperty(Faculty)
    name = db.StringProperty(required=True)
    body = db.TextProperty(required=True)
    image = db.BlobProperty(required=False)


class GroupsOfBlogs(db.Model):
    name = db.StringProperty(required=True)


class Blog(db.Model):
    name = db.StringProperty(required=True)
    content = db.StringProperty(required=True, multiline=True)
    group = db.ReferenceProperty(GroupsOfBlogs, required=True)
    date = db.DateTimeProperty(required=True, auto_now_add=True)
