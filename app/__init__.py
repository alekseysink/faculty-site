# -*- coding: utf-8 -*-
import sys
import os

sys.path.insert(1, os.path.join(os.path.abspath('.'), 'env_faculty/lib/python2.7/site-packages'))

from flask import Flask

app = Flask(__name__)
app.config.from_object('settings')

import views
from app.admin.views import admin as AdminModule
app.register_blueprint(AdminModule)