from app import app
from flask import render_template, redirect, url_for
from models import *

PER_PAGE = 10


@app.route('/')
def index():
    return redirect(url_for('blog'))


@app.route('/faculty/', defaults={'page': 1, 'group': 0})
@app.route('/faculty/group/<int:group>', defaults={'page': 1})
@app.route('/faculty/page/<int:page>', defaults={'group': 0})
@app.route('/faculty/group/<int:group>/page/<int:page>')
def blog(group, page):
    from paginator import Paginator, InvalidPage, EmptyPage

    if group:
        model = Blog.all().filter('group = ', GroupsOfBlogs.get_by_id(group)).fetch(100)
    else:
        model = Blog.all().fetch(100)

    paginator = Paginator(model, 3)

    if page:
        try:
            page = int(page)
        except ValueError:
            page = 1
        try:
            paginator = paginator.page(page)
        except (EmptyPage, InvalidPage):
            paginator = paginator.page(paginator.num_pages)

    return render_template('base.html', group=group, paginator=paginator, current=page, groups=GroupsOfBlogs.all())


@app.route('/faculty/new/<int:id_group>')
def news(id_group):

    return render_template('news.html', xyu=Blog.get_by_id(id_group).content)
